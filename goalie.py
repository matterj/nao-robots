"""
Nao Projects Group: Lukas, Albin, Revan, Jan:

Description of the script
In this goalie script we implemented a goalkeeper handling which allows the robot to move sidewards towards the ball direction and as soon as the ball is near enough it crunches into a goalkeeper position.
For the movement towards the ball we relied on the tracking service provided by the Naoqi API. In order to handle ball tracking due to camera range restrictions, we implemented a switch of the camera as soon as the tracking is lost.
With the function get offset we use the tracking service to measure the relative position to the ball. If the x_treshhold is reached the robot crunches. If not it checks if the y position is larger than the treshhold and if yes it moves into
the y direction of the ball. The crunch function contains the kick animation we implemented using choreograph.

"""

import dis
from turtle import distance
import qi
import argparse
import sys
import time


def main(session, ballSize):
    """
    This example shows how to use ALTracker with red ball.
    """
    # Get the services ALTracker, ALMotion and ALRobotPosture.

    motion_service = session.service("ALMotion")
    posture_service = session.service("ALRobotPosture")
    tracker_service = session.service("ALTracker")
    vision_service = session.service("ALVideoDevice")
    awareness_service = session.service("ALBasicAwareness")
    
    # First, wake up.
    motion_service.wakeUp()
    awareness_service.pauseAwareness()
    fractionMaxSpeed = 0.8
    # Go to posture stand
    posture_service.goToPosture("StandInit", fractionMaxSpeed)

    effectorName = "Head"
    isEnabled = True
    motion_service.wbEnableEffectorControl(effectorName, isEnabled)
    targetCoordinate = [0.0, 0.0, 0.0]
    motion_service.wbSetEffectorControl(effectorName, targetCoordinate)
    # Add target to track.
    targetName = "RedBall"
    diameterOfBall = ballSize
    tracker_service.registerTarget(targetName, diameterOfBall)

    # set mode
    mode = "Head"
    tracker_service.setMode(mode)

    

    print "ALTracker successfully started, now show a red ball to robot!"
    print "Use Ctrl+c to stop this script."

    tracker_service.track(targetName)
    
    x_treshhold = 1
    y_treshhold = 0.1
    distanceSidewards = 0.1

    try:
        while True:
            # Then, start tracker.

            
            x_offset, y_offset = get_offset(tracker_service)

            if (x_offset < x_treshhold and x_offset != 0):
                print("Goalie")
                motion_service.wbEnableEffectorControl(effectorName, False)
                posture_service.goToPosture("StandInit", fractionMaxSpeed)
                goalie_pose()
                print("pose done")
                #time.sleep(4)
                #motion_service.wbEnableEffectorControl(effectorName, isEnabled)
                tracker_service.track(targetName)
            elif y_offset < -y_treshhold:
                motion_service.moveTo(0, -distanceSidewards, 0)
            elif y_offset > y_treshhold:
                motion_service.moveTo(0, +distanceSidewards, 0)

            if tracker_service.isTargetLost():
                switch_camera(vision_service)
            time.sleep(0.001)
           
        tracker_service.stopTracker()
        tracker_service.unregisterAllTargets()
            # motion_service.moveTo(y=-distanceRightToKick)
            #kick animation   


    except KeyboardInterrupt:
        print
        print "Interrupted by user"
        print "Stopping..."

    # Stop tracker, go to posture Sit.
    tracker_service.stopTracker()
    tracker_service.unregisterAllTargets()
    posture_service.goToPosture("Sit", 0.5)
    motion_service.rest()

    print "ALTracker stopped."

def get_offset(tracker_service):
    position_list = tracker_service.getTargetPosition()
    distances = (position_list[0], position_list[1]) if len(position_list) > 0 else (0, 0)
    print(distances[0], distances[1])
    return distances

def switch_camera(vision_service):
    print "switch camera"
    if vision_service.getActiveCamera() == 0:
        vision_service.setActiveCamera(1)
    else:
        vision_service.setActiveCamera(0)

def goalie_pose():
    # Choregraphe bezier export in Python.
    from naoqi import ALProxy
    names = list()
    times = list()
    keys = list()
    
    names.append("HeadPitch")
    times.append([0.68, 2.6, 3.28])
    keys.append([[-0.14884, [3, -0.24, 0], [3, 0.64, 0]], [-0.144238, [3, -0.64, 0], [3, 0.226667, 0]], [-0.14884, [3, -0.226667, 0], [3, 0, 0]]])
    
    names.append("HeadYaw")
    times.append([0.68, 2.6, 3.28])
    keys.append([[0.00916195, [3, -0.24, 0], [3, 0.64, 0]], [-0.00157595, [3, -0.64, 0.00528635], [3, 0.226667, -0.00187225]], [-0.0123138, [3, -0.226667, 0], [3, 0, 0]]])
    
    names.append("LAnklePitch")
    times.append([0.68, 1.8, 2.6, 3.28])
    keys.append([[0.079726, [3, -0.24, 0], [3, 0.373333, 0]], [0.145688, [3, -0.373333, 0], [3, 0.266667, 0]], [-0.912772, [3, -0.266667, 0], [3, 0.226667, 0]], [-0.480184, [3, -0.226667, 0], [3, 0, 0]]])
    
    names.append("LAnkleRoll")
    times.append([0.68, 1.8, 2.6, 3.28])
    keys.append([[-0.101202, [3, -0.24, 0], [3, 0.373333, 0]], [-0.168698, [3, -0.373333, 0], [3, 0.266667, 0]], [-0.159494, [3, -0.266667, 0], [3, 0.226667, 0]], [-0.352778, [3, -0.226667, 0], [3, 0, 0]]])
    
    names.append("LElbowRoll")
    times.append([0.68, 2.6, 3.28])
    keys.append([[-0.547596, [3, -0.24, 0], [3, 0.64, 0]], [-0.567538, [3, -0.64, 0], [3, 0.226667, 0]], [-0.0935321, [3, -0.226667, 0], [3, 0, 0]]])
    
    names.append("LElbowYaw")
    times.append([0.68, 2.6, 3.28])
    keys.append([[-1.18889, [3, -0.24, 0], [3, 0.64, 0]], [-1.21344, [3, -0.64, 0.0245444], [3, 0.226667, -0.00869279]], [-1.51717, [3, -0.226667, 0], [3, 0, 0]]])
    
    names.append("LHand")
    times.append([0.68, 2.6, 3.28])
    keys.append([[0.3128, [3, -0.24, 0], [3, 0.64, 0]], [0.3128, [3, -0.64, 0], [3, 0.226667, 0]], [0.2964, [3, -0.226667, 0], [3, 0, 0]]])
    
    names.append("LHipPitch")
    times.append([0.68, 1.8, 2.6, 3.28])
    keys.append([[0.168782, [3, -0.24, 0], [3, 0.373333, 0]], [-0.11961, [3, -0.373333, 0.182546], [3, 0.266667, -0.13039]], [-0.770026, [3, -0.266667, 0.25539], [3, 0.226667, -0.217082]], [-1.53703, [3, -0.226667, 0], [3, 0, 0]]])
    
    names.append("LHipRoll")
    times.append([0.68, 1.8, 2.6, 3.28])
    keys.append([[0.09515, [3, -0.24, 0], [3, 0.373333, 0]], [0.09515, [3, -0.373333, 0], [3, 0.266667, 0]], [0.211734, [3, -0.266667, -0.0400775], [3, 0.226667, 0.0340659]], [0.31758, [3, -0.226667, 0], [3, 0, 0]]])
    
    names.append("LHipYawPitch")
    times.append([0.68, 1.8, 2.6, 3.28])
    keys.append([[-0.164096, [3, -0.24, 0], [3, 0.373333, 0]], [-0.171766, [3, -0.373333, 0], [3, 0.266667, 0]], [-0.147222, [3, -0.266667, 0], [3, 0.226667, 0]], [-0.361982, [3, -0.226667, 0], [3, 0, 0]]])
    
    names.append("LKneePitch")
    times.append([0.68, 1.8, 2.6, 3.28])
    keys.append([[-0.0997519, [3, -0.24, 0], [3, 0.373333, 0]], [-0.104354, [3, -0.373333, 0], [3, 0.266667, 0]], [1.79014, [3, -0.266667, -0.384402], [3, 0.226667, 0.326742]], [2.11688, [3, -0.226667, 0], [3, 0, 0]]])
    
    names.append("LShoulderPitch")
    times.append([0.68, 2.6, 3.28])
    keys.append([[1.48794, [3, -0.24, 0], [3, 0.64, 0]], [1.43425, [3, -0.64, 0.0219008], [3, 0.226667, -0.00775653]], [1.39897, [3, -0.226667, 0], [3, 0, 0]]])
    
    names.append("LShoulderRoll")
    times.append([0.68, 2.6, 3.28])
    keys.append([[0.207048, [3, -0.24, 0], [3, 0.64, 0]], [0.276078, [3, -0.64, -0.06903], [3, 0.226667, 0.0244481]], [0.638102, [3, -0.226667, 0], [3, 0, 0]]])
    
    names.append("LWristYaw")
    times.append([0.68, 2.6, 3.28])
    keys.append([[0.139552, [3, -0.24, 0], [3, 0.64, 0]], [0.118076, [3, -0.64, 0], [3, 0.226667, 0]], [0.121144, [3, -0.226667, 0], [3, 0, 0]]])
    
    names.append("RAnklePitch")
    times.append([0.68, 1.8, 2.6, 3.28])
    keys.append([[0.0874801, [3, -0.24, 0], [3, 0.373333, 0]], [0.139636, [3, -0.373333, 0], [3, 0.266667, 0]], [-0.782298, [3, -0.266667, 0], [3, 0.226667, 0]], [-0.515382, [3, -0.226667, 0], [3, 0, 0]]])
    
    names.append("RAnkleRoll")
    times.append([0.68, 1.8, 2.6, 3.28])
    keys.append([[0.101286, [3, -0.24, 0], [3, 0.373333, 0]], [0.260822, [3, -0.373333, -0.0471279], [3, 0.266667, 0.0336628]], [0.343658, [3, -0.266667, -0.0221117], [3, 0.226667, 0.018795]], [0.383542, [3, -0.226667, 0], [3, 0, 0]]])
    
    names.append("RElbowRoll")
    times.append([0.68, 2.6, 3.28])
    keys.append([[0.47865, [3, -0.24, 0], [3, 0.64, 0]], [0.495524, [3, -0.64, 0], [3, 0.226667, 0]], [0.0844119, [3, -0.226667, 0], [3, 0, 0]]])
    
    names.append("RElbowYaw")
    times.append([0.68, 2.6, 3.28])
    keys.append([[1.29159, [3, -0.24, 0], [3, 0.64, 0]], [1.3192, [3, -0.64, -0.022656], [3, 0.226667, 0.00802402]], [1.38363, [3, -0.226667, 0], [3, 0, 0]]])
    
    names.append("RHand")
    times.append([0.68, 2.6, 3.28])
    keys.append([[0.3292, [3, -0.24, 0], [3, 0.64, 0]], [0.3164, [3, -0.64, 0.0056123], [3, 0.226667, -0.00198769]], [0.3064, [3, -0.226667, 0], [3, 0, 0]]])
    
    names.append("RHipPitch")
    times.append([0.68, 1.8, 2.6, 3.28])
    keys.append([[0.159494, [3, -0.24, 0], [3, 0.373333, 0]], [-0.11816, [3, -0.373333, 0.184634], [3, 0.266667, -0.131881]], [-0.790052, [3, -0.266667, 0.256496], [3, 0.226667, -0.218021]], [-1.54171, [3, -0.226667, 0], [3, 0, 0]]])
    
    names.append("RHipRoll")
    times.append([0.68, 1.8, 2.6, 3.28])
    keys.append([[-0.102736, [3, -0.24, 0], [3, 0.373333, 0]], [-0.283748, [3, -0.373333, 0.0343616], [3, 0.266667, -0.024544]], [-0.308292, [3, -0.266667, 0.014649], [3, 0.226667, -0.0124516]], [-0.36505, [3, -0.226667, 0], [3, 0, 0]]])
    
    names.append("RHipYawPitch")
    times.append([0.68, 1.8, 2.6, 3.28])
    keys.append([[-0.164096, [3, -0.24, 0], [3, 0.373333, 0]], [-0.171766, [3, -0.373333, 0], [3, 0.266667, 0]], [-0.147222, [3, -0.266667, 0], [3, 0.226667, 0]], [-0.361982, [3, -0.226667, 0], [3, 0, 0]]])
    
    names.append("RKneePitch")
    times.append([0.68, 1.8, 2.6, 3.28])
    keys.append([[-0.099668, [3, -0.24, 0], [3, 0.373333, 0]], [-0.130348, [3, -0.373333, 0], [3, 0.266667, 0]], [1.6583, [3, -0.266667, -0.404368], [3, 0.226667, 0.343713]], [2.11389, [3, -0.226667, 0], [3, 0, 0]]])
    
    names.append("RShoulderPitch")
    times.append([0.68, 2.6, 3.28])
    keys.append([[1.48189, [3, -0.24, 0], [3, 0.64, 0]], [1.42973, [3, -0.64, 0], [3, 0.226667, 0]], [1.56012, [3, -0.226667, 0], [3, 0, 0]]])
    
    names.append("RShoulderRoll")
    times.append([0.68, 2.6, 3.28])
    keys.append([[-0.184122, [3, -0.24, 0], [3, 0.64, 0]], [-0.257754, [3, -0.64, 0.0736318], [3, 0.226667, -0.0260779]], [-0.667332, [3, -0.226667, 0], [3, 0, 0]]])
    
    names.append("RWristYaw")
    times.append([0.68, 2.6, 3.28])
    keys.append([[0.0643861, [3, -0.24, 0], [3, 0.64, 0]], [0.075124, [3, -0.64, -0.0064192], [3, 0.226667, 0.00227347]], [0.0904641, [3, -0.226667, 0], [3, 0, 0]]])
    
    try:
        # uncomment the following line and modify the IP if you use this script outside Choregraphe.
        # motion = ALProxy("ALMotion", IP, 9559)
        motion = ALProxy("ALMotion")
        motion.angleInterpolationBezier(names, times, keys)
    except BaseException, err:
        print err


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", type=str, default="192.168.43.189",
                        help="Robot IP address. On robot or Local Naoqi: use '127.0.0.1'.")
    parser.add_argument("--port", type=int, default=9559,
                        help="Naoqi port number")
    parser.add_argument("--ballsize", type=float, default=0.13,
                        help="Diameter of ball.")

    args = parser.parse_args()
    session = qi.Session()
    try:
        session.connect("tcp://" + args.ip + ":" + str(args.port))
    except RuntimeError:
        print ("Can't connect to Naoqi at ip \"" + args.ip + "\" on port " + str(args.port) +".\n"
               "Please check your script arguments. Run with -h option for help.")
        sys.exit(1)
    main(session, args.ballsize)