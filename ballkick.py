"""
Nao Projects Group: Lukas, Albin, Revan, Jan:

Description of the script
In this ballkick script we implemented a movement towards the ball with a consequent kick. For the movement towards the ball we relied on the tracking service provided
by the Naoqi API. In order to handle ball tracking due to camera range restrictions, we implemented a switch of the camera as soon as the tracking is lost.
With the function is_ball_reached we use the tracking service to measure the distance to the ball and as soon as the treshold x_treshhold is reached the 
function kick is executed. The kick function contains the kick animation we implemented using choreograph.

Open issues:
So far the robot is not able to return to the tracking after the ball is kicked. Furthermore the kick animation could be optimized to provide a faster kick.
The movement to the right before the kick animation was also not implemented yet, which could potentially yield a better shooting precision.

"""

import qi
import argparse
import sys
import time


def main(session, ballSize):
    """
    This example shows how to use ALTracker with red ball.
    """
    # Get the services ALTracker, ALMotion and ALRobotPosture.

    motion_service = session.service("ALMotion")
    posture_service = session.service("ALRobotPosture")
    tracker_service = session.service("ALTracker")
    vision_service = session.service("ALVideoDevice")
    awareness_service = session.service("ALBasicAwareness")
    
    # First, wake up.
    motion_service.wakeUp()
    awareness_service.pauseAwareness()
    fractionMaxSpeed = 0.8
    # Go to posture stand
    posture_service.goToPosture("StandInit", fractionMaxSpeed)

    effectorName = "Head"
    isEnabled = True
    motion_service.wbEnableEffectorControl(effectorName, isEnabled)
    targetCoordinate = [0.0, 0.0, 0.0]
    motion_service.wbSetEffectorControl(effectorName, targetCoordinate)
    # Add target to track.
    targetName = "RedBall"
    diameterOfBall = ballSize
    tracker_service.registerTarget(targetName, diameterOfBall)

    # set mode
    mode = "Move"
    tracker_service.setMode(mode)

    # Then, start tracker.
    tracker_service.track(targetName)

    print "ALTracker successfully started, now show a red ball to robot!"
    print "Use Ctrl+c to stop this script."
    kick()
    kickDist = 0.2
    distanceRightToKick = 0.1

    try:
        while True:  
            while not is_ball_reached(tracker_service, kickDist):
                time.sleep(1)
                if tracker_service.isTargetLost():
                    switch_camera(vision_service)
            tracker_service.stopTracker()

            # motion_service.moveTo(y=-distanceRightToKick)
            #kick animation
            kick()
            print "Going to sleep for 3 sec"
            time.sleep(3)
            print "Finished sleeping"


    except KeyboardInterrupt:
        print
        print "Interrupted by user"
        print "Stopping..."

    # Stop tracker, go to posture Sit.
    tracker_service.stopTracker()
    tracker_service.unregisterAllTargets()
    posture_service.goToPosture("Sit", fractionMaxSpeed)
    motion_service.rest()

    print "ALTracker stopped."

def is_ball_reached(tracker_service, x_treshhold):
    position_list = tracker_service.getTargetPosition()
    x_dist = position_list[0] if len(position_list) > 0 else x_treshhold + 1
    print x_dist
    if x_dist < x_treshhold:
        return True
    return False

def switch_camera(vision_service):
    if vision_service.getActiveCamera() == 0:
        vision_service.setActiveCamera(1)
    else:
        vision_service.setActiveCamera(0)

def kick():
    # Choregraphe bezier export in Python.
    from naoqi import ALProxy
    names = list()
    times = list()
    keys = list()

    names.append("HeadPitch")
    times.append([0.44, 1.36, 2.32, 3.32, 3.56, 5.12])
    keys.append([[-0.343659, [3, -0.16, 0], [3, 0.306667, 0]], [-0.14884, [3, -0.306667, 0], [3, 0.32, 0]], [-0.15651, [3, -0.32, 0], [3, 0.333333, 0]], [-0.147306, [3, -0.333333, 0], [3, 0.08, 0]], [-0.154976, [3, -0.08, 0], [3, 0.52, 0]], [-0.145772, [3, -0.52, 0], [3, 0, 0]]])

    names.append("HeadYaw")
    times.append([0.44, 1.36, 2.32, 3.32, 3.56, 5.12])
    keys.append([[0.205514, [3, -0.16, 0], [3, 0.306667, 0]], [0.00762796, [3, -0.306667, 0], [3, 0.32, 0]], [0.00762796, [3, -0.32, 0], [3, 0.333333, 0]], [-0.00310993, [3, -0.333333, 0], [3, 0.08, 0]], [-0.00310993, [3, -0.08, 0], [3, 0.52, 0]], [-0.0138481, [3, -0.52, 0], [3, 0, 0]]])

    names.append("LAnklePitch")
    times.append([0.44, 1.36, 2.32, 3.32, 3.56, 5.12])
    keys.append([[0.0843279, [3, -0.16, 0], [3, 0.306667, 0]], [0.0966001, [3, -0.306667, 0], [3, 0.32, 0]], [0.0904641, [3, -0.32, 0.00613606], [3, 0.333333, -0.00639173]], [-0.343659, [3, -0.333333, 0], [3, 0.08, 0]], [-0.2102, [3, -0.08, -0.019158], [3, 0.52, 0.124527]], [0.0873961, [3, -0.52, 0], [3, 0, 0]]])

    names.append("LAnkleRoll")
    times.append([0.44, 1.36, 2.32, 3.32, 3.56, 5.12])
    keys.append([[-0.11194, [3, -0.16, 0], [3, 0.306667, 0]], [-0.116542, [3, -0.306667, 0], [3, 0.32, 0]], [-0.108872, [3, -0.32, -0.0076702], [3, 0.333333, 0.0079898]], [0.0353239, [3, -0.333333, 0], [3, 0.08, 0]], [-0.030638, [3, -0.08, 0.00654507], [3, 0.52, -0.042543]], [-0.11194, [3, -0.52, 0], [3, 0, 0]]])

    names.append("LElbowRoll")
    times.append([0.44, 1.36, 2.32, 3.32, 3.56, 5.12])
    keys.append([[-0.392662, [3, -0.16, 0], [3, 0.306667, 0]], [-0.352778, [3, -0.306667, 0], [3, 0.32, 0]], [-0.477032, [3, -0.32, 0], [3, 0.333333, 0]], [-0.266875, [3, -0.333333, -0.0610302], [3, 0.08, 0.0146473]], [-0.249999, [3, -0.08, 0], [3, 0.52, 0]], [-0.408002, [3, -0.52, 0], [3, 0, 0]]])

    names.append("LElbowYaw")
    times.append([0.44, 1.36, 2.32, 3.32, 3.56, 5.12])
    keys.append([[-1.18582, [3, -0.16, 0], [3, 0.306667, 0]], [-1.17202, [3, -0.306667, -0.0125114], [3, 0.32, 0.0130554]], [-1.10912, [3, -0.32, -0.0175314], [3, 0.333333, 0.0182619]], [-1.06464, [3, -0.333333, 0], [3, 0.08, 0]], [-1.07538, [3, -0.08, 0.00504517], [3, 0.52, -0.0327936]], [-1.17815, [3, -0.52, 0], [3, 0, 0]]])

    names.append("LHand")
    times.append([0.44, 1.36, 2.32, 3.32, 3.56, 5.12])
    keys.append([[0.2852, [3, -0.16, 0], [3, 0.306667, 0]], [0.2848, [3, -0.306667, 0], [3, 0.32, 0]], [0.2848, [3, -0.32, 0], [3, 0.333333, 0]], [0.2928, [3, -0.333333, -0.00166657], [3, 0.08, 0.000399977]], [0.2932, [3, -0.08, 0], [3, 0.52, 0]], [0.286, [3, -0.52, 0], [3, 0, 0]]])

    names.append("LHipPitch")
    times.append([0.44, 1.36, 2.32, 3.32, 3.56, 5.12])
    keys.append([[0.127364, [3, -0.16, 0], [3, 0.306667, 0]], [0.127364, [3, -0.306667, 0], [3, 0.32, 0]], [0.124296, [3, -0.32, 0], [3, 0.333333, 0]], [0.477115, [3, -0.333333, 0], [3, 0.08, 0]], [-0.521518, [3, -0.08, 0], [3, 0.52, 0]], [0.12583, [3, -0.52, 0], [3, 0, 0]]])

    names.append("LHipRoll")
    times.append([0.44, 1.36, 2.32, 3.32, 3.56, 5.12])
    keys.append([[0.115092, [3, -0.16, 0], [3, 0.306667, 0]], [0.130432, [3, -0.306667, 0], [3, 0.32, 0]], [0.121228, [3, -0.32, 0], [3, 0.333333, 0]], [0.167248, [3, -0.333333, 0], [3, 0.08, 0]], [0.0414601, [3, -0.08, 0], [3, 0.52, 0]], [0.116626, [3, -0.52, 0], [3, 0, 0]]])

    names.append("LHipYawPitch")
    times.append([0.44, 1.36, 2.32, 3.32, 3.56, 5.12])
    keys.append([[-0.16563, [3, -0.16, 0], [3, 0.306667, 0]], [-0.170232, [3, -0.306667, 0.00100089], [3, 0.32, -0.0010444]], [-0.171766, [3, -0.32, 0.00153396], [3, 0.333333, -0.00159788]], [-0.483456, [3, -0.333333, 0], [3, 0.08, 0]], [-0.197844, [3, -0.08, -0.00330404], [3, 0.52, 0.0214763]], [-0.176367, [3, -0.52, 0], [3, 0, 0]]])

    names.append("LKneePitch")
    times.append([0.44, 1.36, 2.32, 3.32, 3.56, 5.12])
    keys.append([[-0.090548, [3, -0.16, 0], [3, 0.306667, 0]], [-0.0890141, [3, -0.306667, 0], [3, 0.32, 0]], [-0.093616, [3, -0.32, 0], [3, 0.333333, 0]], [-0.067538, [3, -0.333333, -0.026078], [3, 0.08, 0.00625872]], [0.688724, [3, -0.08, 0], [3, 0.52, 0]], [-0.10282, [3, -0.52, 0], [3, 0, 0]]])

    names.append("LShoulderPitch")
    times.append([0.44, 1.36, 2.32, 3.32, 3.56, 5.12])
    keys.append([[1.50788, [3, -0.16, 0], [3, 0.306667, 0]], [1.50174, [3, -0.306667, 0], [3, 0.32, 0]], [1.50941, [3, -0.32, -0.00751359], [3, 0.333333, 0.00782666]], [1.54776, [3, -0.333333, 0], [3, 0.08, 0]], [1.54776, [3, -0.08, 0], [3, 0.52, 0]], [1.51402, [3, -0.52, 0], [3, 0, 0]]])

    names.append("LShoulderRoll")
    times.append([0.44, 1.36, 2.32, 3.32, 3.56, 5.12])
    keys.append([[0.161028, [3, -0.16, 0], [3, 0.306667, 0]], [0.110406, [3, -0.306667, 0], [3, 0.32, 0]], [1.28545, [3, -0.32, 0], [3, 0.333333, 0]], [0.449421, [3, -0.333333, 0.0894846], [3, 0.08, -0.0214763]], [0.427944, [3, -0.08, 0.0123402], [3, 0.52, -0.0802113]], [0.171766, [3, -0.52, 0], [3, 0, 0]]])

    names.append("LWristYaw")
    times.append([0.44, 1.36, 2.32, 3.32, 3.56, 5.12])
    keys.append([[0.0996681, [3, -0.16, 0], [3, 0.306667, 0]], [0.13495, [3, -0.306667, 0], [3, 0.32, 0]], [0.125746, [3, -0.32, 0], [3, 0.333333, 0]], [0.139552, [3, -0.333333, -0.00618546], [3, 0.08, 0.00148451]], [0.148756, [3, -0.08, -0.000613596], [3, 0.52, 0.00398837]], [0.153358, [3, -0.52, 0], [3, 0, 0]]])

    names.append("RAnklePitch")
    times.append([0.44, 1.36, 2.32, 3.32, 3.56, 5.12])
    keys.append([[0.0798099, [3, -0.16, 0], [3, 0.306667, 0]], [0.0123138, [3, -0.306667, 0], [3, 0.32, 0]], [0.016916, [3, -0.32, -0.00100183], [3, 0.333333, 0.00104357]], [0.01845, [3, -0.333333, -0.001534], [3, 0.08, 0.000368161]], [0.030722, [3, -0.08, -0.00354524], [3, 0.52, 0.023044]], [0.0982179, [3, -0.52, 0], [3, 0, 0]]])

    names.append("RAnkleRoll")
    times.append([0.44, 1.36, 2.32, 3.32, 3.56, 5.12])
    keys.append([[0.104354, [3, -0.16, 0], [3, 0.306667, 0]], [-0.14262, [3, -0.306667, 0.061806], [3, 0.32, -0.0644932]], [-0.274544, [3, -0.32, 0.00883669], [3, 0.333333, -0.00920488]], [-0.283749, [3, -0.333333, 0], [3, 0.08, 0]], [-0.274544, [3, -0.08, -0.00920488], [3, 0.52, 0.0598317]], [0.112024, [3, -0.52, 0], [3, 0, 0]]])

    names.append("RElbowRoll")
    times.append([0.44, 1.36, 2.32, 3.32, 3.56, 5.12])
    keys.append([[0.401949, [3, -0.16, 0], [3, 0.306667, 0]], [0.07214, [3, -0.306667, 0], [3, 0.32, 0]], [0.176453, [3, -0.32, 0], [3, 0.333333, 0]], [0.154976, [3, -0.333333, 0.00865988], [3, 0.08, -0.00207837]], [0.144238, [3, -0.08, 0], [3, 0.52, 0]], [0.397349, [3, -0.52, 0], [3, 0, 0]]])

    names.append("RElbowYaw")
    times.append([0.44, 1.36, 2.32, 3.32, 3.56, 5.12])
    keys.append([[1.20722, [3, -0.16, 0], [3, 0.306667, 0]], [1.26704, [3, -0.306667, 0], [3, 0.32, 0]], [1.2471, [3, -0.32, 0.0057603], [3, 0.333333, -0.00600031]], [1.23176, [3, -0.333333, 0], [3, 0.08, 0]], [1.25017, [3, -0.08, 0], [3, 0.52, 0]], [1.175, [3, -0.52, 0], [3, 0, 0]]])

    names.append("RHand")
    times.append([0.44, 1.36, 2.32, 3.32, 3.56, 5.12])
    keys.append([[0.312, [3, -0.16, 0], [3, 0.306667, 0]], [0.2884, [3, -0.306667, 0.00459999], [3, 0.32, -0.00479999]], [0.2836, [3, -0.32, 0.000768013], [3, 0.333333, -0.000800014]], [0.2828, [3, -0.333333, 0], [3, 0.08, 0]], [0.29, [3, -0.08, 0], [3, 0.52, 0]], [0.29, [3, -0.52, 0], [3, 0, 0]]])

    names.append("RHipPitch")
    times.append([0.44, 1.36, 2.32, 3.32, 3.56, 5.12])
    keys.append([[0.12728, [3, -0.16, 0], [3, 0.306667, 0]], [0.161028, [3, -0.306667, 0], [3, 0.32, 0]], [0.153358, [3, -0.32, 0], [3, 0.333333, 0]], [0.156426, [3, -0.333333, 0], [3, 0.08, 0]], [0.153358, [3, -0.08, 0.00170444], [3, 0.52, -0.0110789]], [0.118076, [3, -0.52, 0], [3, 0, 0]]])

    names.append("RHipRoll")
    times.append([0.44, 1.36, 2.32, 3.32, 3.56, 5.12])
    keys.append([[-0.115008, [3, -0.16, 0], [3, 0.306667, 0]], [0.261799, [3, -0.306667, 0], [3, 0.32, 0]], [0.122173, [3, -0.32, 0], [3, 0.333333, 0]], [0.122762, [3, -0.333333, -0.000588875], [3, 0.08, 0.00014133]], [0.136568, [3, -0.08, 0], [3, 0.52, 0]], [-0.11961, [3, -0.52, 0], [3, 0, 0]]])

    names.append("RHipYawPitch")
    times.append([0.44, 1.36, 2.32, 3.32, 3.56, 5.12])
    keys.append([[-0.16563, [3, -0.16, 0], [3, 0.306667, 0]], [-0.170232, [3, -0.306667, 0.00100089], [3, 0.32, -0.0010444]], [-0.171766, [3, -0.32, 0.00153396], [3, 0.333333, -0.00159788]], [-0.483456, [3, -0.333333, 0], [3, 0.08, 0]], [-0.197844, [3, -0.08, -0.00330404], [3, 0.52, 0.0214763]], [-0.176367, [3, -0.52, 0], [3, 0, 0]]])

    names.append("RKneePitch")
    times.append([0.44, 1.36, 2.32, 3.32, 3.56, 5.12])
    keys.append([[-0.0873961, [3, -0.16, 0], [3, 0.306667, 0]], [-0.0996681, [3, -0.306667, 0], [3, 0.32, 0]], [-0.0981341, [3, -0.32, -0.0010018], [3, 0.333333, 0.00104355]], [-0.093532, [3, -0.333333, -0.00460208], [3, 0.08, 0.0011045]], [-0.078192, [3, -0.08, 0], [3, 0.52, 0]], [-0.101202, [3, -0.52, 0], [3, 0, 0]]])

    names.append("RShoulderPitch")
    times.append([0.44, 1.36, 2.32, 3.32, 3.56, 5.12])
    keys.append([[1.50336, [3, -0.16, 0], [3, 0.306667, 0]], [1.5816, [3, -0.306667, 0], [3, 0.32, 0]], [1.50796, [3, -0.32, 0], [3, 0.333333, 0]], [1.55859, [3, -0.333333, -0.0222675], [3, 0.08, 0.0053442]], [1.5908, [3, -0.08, 0], [3, 0.52, 0]], [1.5049, [3, -0.52, 0], [3, 0, 0]]])

    names.append("RShoulderRoll")
    times.append([0.44, 1.36, 2.32, 3.32, 3.56, 5.12])
    keys.append([[-0.162646, [3, -0.16, 0], [3, 0.306667, 0]], [-0.414222, [3, -0.306667, 0], [3, 0.32, 0]], [-0.0414601, [3, -0.32, 0], [3, 0.333333, 0]], [-0.0583338, [3, -0.333333, 0.00783493], [3, 0.08, -0.00188038]], [-0.0706061, [3, -0.08, 0.00484064], [3, 0.52, -0.0314641]], [-0.167248, [3, -0.52, 0], [3, 0, 0]]])

    names.append("RWristYaw")
    times.append([0.44, 1.36, 2.32, 3.32, 3.56, 5.12])
    keys.append([[0.156426, [3, -0.16, 0], [3, 0.306667, 0]], [0.0674542, [3, -0.306667, 0.00294028], [3, 0.32, -0.00306812]], [0.0643861, [3, -0.32, 0], [3, 0.333333, 0]], [0.0643861, [3, -0.333333, 0], [3, 0.08, 0]], [0.0643861, [3, -0.08, 0], [3, 0.52, 0]], [0.0689882, [3, -0.52, 0], [3, 0, 0]]])

    try:
        # uncomment the following line and modify the IP if you use this script outside Choregraphe.
        motion = ALProxy("ALMotion", "192.168.38.213", 9559)
        #motion = ALProxy("ALMotion")
        motion.angleInterpolationBezier(names, times, keys)
    except BaseException, err:
        print err


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", type=str, default="192.168.38.213",
                        help="Robot IP address. On robot or Local Naoqi: use '127.0.0.1'.")
    parser.add_argument("--port", type=int, default=9559,
                        help="Naoqi port number")
    parser.add_argument("--ballsize", type=float, default=0.13,
                        help="Diameter of ball.")

    args = parser.parse_args()
    session = qi.Session()
    try:
        session.connect("tcp://" + args.ip + ":" + str(args.port))
    except RuntimeError:
        print ("Can't connect to Naoqi at ip \"" + args.ip + "\" on port " + str(args.port) +".\n"
               "Please check your script arguments. Run with -h option for help.")
        sys.exit(1)
    main(session, args.ballsize)
